# testing email while using tailscale
import email
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
import socket

def emailMessage(subjectLine, messageBody):    
    fromAddress = "mattronome1@gmail.com"
    # recipients = ['matt@mac.com', 'smattmann@mac.com']
    recipients = ['matt@mac.com']

    msg = MIMEMultipart()
    msg['From'] = fromAddress
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = subjectLine

    body = messageBody
    msg.attach(MIMEText(body, 'plain'))
    
    try:
        server = None
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("mattronome1@gmail.com", "peblpxstyceqtmpv")
        text = msg.as_string()
        server.sendmail(fromAddress, recipients, text)
    except smtplib.SMTPException as e:
        print('Error: Unable to send email: %s\n' % e)
    except socket.gaierror as e:
        print('Error: Unable to send email. Socket error: %s\n' % e)
    finally:
        if (server is not None):
            server.quit()

def main():
    subjectLine = "Email test"
    messageBody = "payload"
    emailMessage(subjectLine, messageBody)


# make main() the entry point of this script
if __name__ == '__main__':
    main()



# socket.gaierror: [Errno -3] Temporary failure in name resolution
