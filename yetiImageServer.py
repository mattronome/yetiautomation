#!/usr/bin/env python3

import os
import time
import tornado.ioloop
import tornado.web
from tornado.web import StaticFileHandler
from datetime import datetime as dt
import logging
from logging import FileHandler
from tornado.log import enable_pretty_logging

#----------------------------Globals-----------------------------
pngFile = 'currentYetiSystemStatus.png'
basedir = os.path.dirname(os.path.realpath(__file__))
imagedir = os.path.join(basedir, 'images')
imagePath = imagedir + "/" + pngFile

#----------------------------Logging-----------------------------
handler = logging.FileHandler('tornadoLogFile.log')
app_log = logging.getLogger("tornado.application")
enable_pretty_logging()
app_log.addHandler(handler)


def file_mod_date(path_to_file):
    time_stamp = time.ctime(os.path.getmtime(path_to_file))
    return time_stamp

class MyStaticFileHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        # Disable cache
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')

class MainHandler(tornado.web.RequestHandler):
    global imagePath
    def get(self):
        # we used to pass the mod date in as a param to the template:
        # mod_date = file_mod_date(imagePath)
        app_log.info("Page request at: {timestamp}".format(timestamp=dt.today().strftime("%Y/%m/%d %H:%M:%S.%f")))
        self.render("yetiStatusTemplate.html")
         
def make_app():
    global imagedir
    
    app_log.info("looking for images in %s", imagedir)
    return tornado.web.Application([
        (r"/", MainHandler),
        (r'/images/(.*)', MyStaticFileHandler, {'path':imagedir}),
    ])
 
if __name__ == "__main__":
    print('Starting server')
    app = make_app()
    app.listen(8585)
    tornado.ioloop.IOLoop.instance().start()


""" 
   Example of passing value to template: 
   In code:
    self.render("waterUsageTemplate.html", image_timestamp=mod_date)
   In template:
   <p><font size="-1">(Current usage chart generated: {{image_timestamp}})</font></p> 
   Not currently used since timestamps are now part of image
"""
