#!/usr/bin/env python3

import RPi.GPIO as GPIO
from time import sleep

MANUAL_OVERRIDE_SETTING = "Auto" # states are "Auto", "Dumpload", "Yeti" to direct output


# This is the GPIO pin number 
DUMP_LOAD_RELAY = 26
MANUAL_OVERRIDE_DUMPLOAD_SWITCH = 9 # One side of three-way switch, off implies Auto
MANUAL_OVERRIDE_YETI_SWITCH = 10    # Other side of three-way switch, off implies Auto

def setupGPIO():
    # set-up input pin and interrupt
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(DUMP_LOAD_RELAY, GPIO.OUT)
    GPIO.setup(MANUAL_OVERRIDE_DUMPLOAD_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(MANUAL_OVERRIDE_DUMPLOAD_SWITCH, GPIO.BOTH, callback=manualOverrideSwitchAction, bouncetime=100)
    GPIO.setup(MANUAL_OVERRIDE_YETI_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(MANUAL_OVERRIDE_YETI_SWITCH, GPIO.BOTH, callback=manualOverrideSwitchAction, bouncetime=100)

def manualOverrideSwitchAction(channel):
    global MANUAL_OVERRIDE_SETTING
    
    if GPIO.input(MANUAL_OVERRIDE_DUMPLOAD_SWITCH):
        MANUAL_OVERRIDE_SETTING = "Dumpload"
        print("MANUAL_OVERRIDE_SETTING is Dumpload ")
    elif GPIO.input(MANUAL_OVERRIDE_YETI_SWITCH):
        MANUAL_OVERRIDE_SETTING = "Yeti"
        print("MANUAL_OVERRIDE_SETTING is Yeti")
    else:
        MANUAL_OVERRIDE_SETTING = "Auto"
        print("MANUAL_OVERRIDE_SETTING is Auto")
        
def main():
    global USE_DUMP_LOAD

    try:
        setupGPIO()
        print("program started...")
        print("Initial setting for MANUAL_OVERRIDE_SETTING is: ", MANUAL_OVERRIDE_SETTING)

        while (True):
            sleep(1)
    

    finally:
        GPIO.cleanup()


# make main() the entry point of this script
if __name__ == '__main__':
    main()

