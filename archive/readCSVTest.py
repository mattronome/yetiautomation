#!/usr/bin/env python3

import os
import os.path
import sys
import time
from time import sleep
import pandas as pd
from datetime import datetime, timedelta

# log file in ./solarYetiLogs
basedir = os.path.dirname(os.path.realpath(__file__))
csvfile = 'currentSystemStatus.log'
logDir = os.path.join(basedir, 'yetiSystemLogs')
logPath = logDir + "/" + csvfile

def main():

    while True:
        try: 
            df = pd.read_csv(logPath, parse_dates=['DateAndTime'])
            print(df)
    
        except (FileNotFoundError):
            print("Log file not found..)

        time.sleep(780) # every 13 minutes, to offset from system logger



# make main() the entry point of this script
if __name__ == '__main__':
    main()

