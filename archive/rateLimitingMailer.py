import logging.handlers
import time
from ratelimitingfilter import RateLimitingFilter

emailLogger = logging.getLogger('throttled_smtp_example')


def emailMessageWithRateLimit(subjectLine, messageBody, rate=1, per=60):
    global emailLogger

    # Create an SMTPHandler
    smtp = logging.handlers.SMTPHandler(
        mailhost=('smtp.gmail.com', 587),
        fromaddr='mattronome1@gmail.com',
        toaddrs='matt@mac.com',
        subject=subjectLine,
        credentials=('mattronome1@gmail.com', 'peblpxstyceqtmpv'),
        secure=()
    )
    smtp.setLevel(logging.ERROR)

    # Create a formatter and set it on the handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    smtp.setFormatter(formatter)

    # Create an instance of the RateLimitingFilter, and add it to the handler
    ratelimit = RateLimitingFilter(rate=1, per=60) # limit to one message per 60 seconds
    smtp.addFilter(ratelimit)

    # Add the handler to the emailLogger
    emailLogger.addHandler(smtp)
    emailLogger.error(messageBody)

i = 1

# Logged errors will now be restricted to 1 every 30 seconds
while True:
    emailMessageWithRateLimit("Yeti rate limited mailer test", "Lost Wifi contact")
    print ("Called emailMessageWithRateLimit(): interation: {i}")
    i = i+1
    time.sleep(2)