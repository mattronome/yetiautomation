#!/usr/bin/env python3

import os
import os.path
import sys
import time
from time import sleep
import pandas as pd
from datetime import datetime, timedelta
import matplotlib
import matplotlib.units as units
import matplotlib.ticker as ticker
import matplotlib.dates as mdates
import matplotlib.pyplot as plt

# ToDo
# On startup, this warning appears. Need to look into using new converters
# Creating Yeti status images...
# /usr/local/lib/python3.6/site-packages/pandas/plotting/_converter.py:129: FutureWarning: Using an implicitly registered datetime converter for a matplotlib plotting method. The converter was registered by pandas on import. Future versions of pandas will require you to explicitly register matplotlib converters.
# 
# To register the converters:
# 	>>> from pandas.plotting import register_matplotlib_converters
# 	>>> register_matplotlib_converters()
#   warnings.warn(msg, FutureWarning)
# 
#
#----------------------------global variables-----------------------------
# headerString ='DateAndTime,PercentCharged,AmpsIn,AmpsOut'
dt = None
dates = None
batteryPercent = None
wattsIn = None
wattsOut = None
basedir = os.path.dirname(os.path.realpath(__file__))

# cookie created by shedControl when it rotates that status log. Tells us to 
# create 'yesterdaysSolarSystemStatus.png' Matt: should move to shared import file
cookieFilePath = os.path.join(basedir, 'createYesterdaySummaryImage')

# log file in ./solarYetiLogs
csvfile = 'currentSystemStatus.log'
logDir = os.path.join(basedir, 'yetiSystemLogs')
logPath = logDir + "/" + csvfile

# image file in ./images
pngFile = 'currentYetiSystemStatus.png'
yesterdaysPNGFile = 'yesterdaysYetiSystemStatus.png'
imageDir = os.path.join(basedir, 'images')
currentDayImagePath = imageDir + "/" + pngFile
yesterdaysImagePath = imageDir + "/" + yesterdaysPNGFile

#----------------------------Routines-----------------------------
def readSystemStatusData(logPath):
    global dt, dates, batteryPercent, wattsIn, wattsOut

    df = pd.read_csv(logPath, parse_dates=['DateAndTime'])
    dt= df['DateAndTime']
    batteryPercent = df['PercentCharged']
    wattsIn = df['WattsIn']
    wattsOut = df['WattsOut']
    
    # convert panda dates to python datetimes
    dates=[ts.to_pydatetime() for ts in dt]
    
def generateSystemStatusImage(imagePath, imageTitle):
    global dt, dates, wattsIn, wattsOut, batteryPercent
    
    ######## Set frame size ###########
    # Get current size, default is [8.0, 6.0]
    fig_size = plt.rcParams["figure.figsize"]
 
    # Set figure width wider and taller
    fig_size[0] = 15
    fig_size[1] = 10
    plt.rcParams["figure.figsize"] = fig_size

    ########
    fig, ax1 = plt.subplots(1)
    
    p1, = ax1.plot(dates, wattsIn, 'g-', label='Watts In')
    p3, = ax1.plot(dates, wattsOut, 'r-', label='Watts Out')
    
    today = datetime.today()
    todaysDate = today.strftime("%Y/%m/%d %H:%M:%S")
 
    plt.suptitle(imageTitle, x=0.52, y=0.98, fontsize=18) #x=0.5, y=0.98 are defaults
    plt.title('As of: ' + todaysDate, fontsize=10)

    xtick_locator = mdates.AutoDateLocator()
    xtick_formatter = mdates.AutoDateFormatter(xtick_locator)
    ax1.xaxis.set_major_locator(xtick_locator)
    ax1.xaxis.set_major_formatter(xtick_formatter)

    ax1.set_xlabel('Time')
    # Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylabel('Watts In/Out', color='k')
    ax1.tick_params('y', colors='k')
    ax1.set_ylim(bottom=0, top=600)
    
    ax2 = ax1.twinx()
    ax2.grid(True)
    p2, = ax2.plot(dt, batteryPercent, 'b-', linewidth=2.0, label='Battery Percent')
    ax2.fill_between(dates, 0, batteryPercent, alpha=.1, color='b')
    ax2.set_ylabel('Battery Percent', color='b')
    ax2.tick_params('y', colors='b')
    ax2.set_ylim(bottom=0)

    fig.autofmt_xdate() # displays datetime diagonally
    
    # legend
    lines = [p1,p2,p3]
    ax1.legend(lines, [l.get_label() for l in lines], bbox_to_anchor=(0.15,1.10))

    # plt.show() # for testing
    fig.savefig(imagePath, format='png', transparent=False, dpi=80, bbox_inches="tight")
    plt.close('all') # otherwise figures remain open and use up memory
    
def yesterdaysLogPath():
    global logPath
    yesterday = datetime.today() - timedelta(1)
    yesterdaysDateFileStamp = yesterday.strftime("%Y-%m-%d")
    yesterdaysLogPath = logPath + "." + yesterdaysDateFileStamp
    return yesterdaysLogPath

def cookieFileExists():
    global cookieFilePath
    return os.path.isfile(cookieFilePath)
  
def removeCookieFile():
    global cookieFilePath
    os.remove(cookieFilePath)

def createSolarSystemStatusImageFile():
    global currentDayImagePath, yesterdaysImagePath, logPath
    
    print("Creating Yeti status images...")
    readSystemStatusData(logPath)
    generateSystemStatusImage(currentDayImagePath, 'Yeti Status Today')
    
    if (cookieFileExists()):
        readSystemStatusData(yesterdaysLogPath())
        generateSystemStatusImage(yesterdaysImagePath, 'Yeti Status Yesterday')
        removeCookieFile()

    print("...done")
    
def makeDirIfNeeded(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

def main():
    global imageDir
    print("Yeti status image generator started...")
    makeDirIfNeeded(imageDir)
    
    while True:
        try: 
            createSolarSystemStatusImageFile()
    
        except (FileNotFoundError):
            print("Log file not found...")

        time.sleep(780) # every 13 minutes, to offset from system logger


# make main() the entry point of this script
if __name__ == '__main__':
    main()
