import json
import requests
import subprocess
import logging
from enum import Enum
from gpiozero import OutputDevice
import time
import collections

SOC_DEQUE_CONTAINER_LEN = 5 # average x readings of state of charge to nominal state


###############################################################################
#### Classes
# command string for all ports
#         commandString = "curl --location --request POST \"http://172.16.42.20/state\" \
#          --header \"Content-Type: application/json\" \
#          --data \"{\"acPortStatus\": 1,\"v12PortStatus\": 0,\"usbPortStatus\": 0}\""

class YetiPort(Enum):
    USB = 1
    V12= 2
    AC = 3

class Yeti1400:
    def __init__(self, thingName, socPercent, volts, timestamp, isCharging, temperature, \
                       usbPortStatus, acPortStatus, v12PortStatus, ampsIn, ampsOut, wattsIn, wattsOut, *args, **kwargs):
        self.thingName = thingName
        self.socPercent = socPercent
        self.volts = volts
        self.timestamp = timestamp
        self.isCharging = isCharging
        self.temperature = temperature
        self.usbPortStatus = usbPortStatus
        self.acPortStatus = acPortStatus
        self.v12PortStatus = v12PortStatus
        self.ampsIn = ampsIn
        self.ampsOut = ampsOut
        self.wattsIn = wattsIn
        self.wattsOut = wattsOut
        self.debugLogger = None
        self.averageSOCPercent = 50 # picking an initial and fictional charge level for startup
        self.socDequeContainer = None

    # can be numeric or domain name, eg. 172.16.42.20 or foo.updog.co
    def setURLDomain(self, urlDomain):
        self.urlDomain = urlDomain
        
    def setDebugLogger(self, logger):
        self.debugLogger = logger
        
    def logAndPrintMsg(self, msg):
        self.debugLogger.info(msg)
        print(msg)

    def loadIsOn(self):
        self.updateStatus
        if (bool(self.usbPortStatus) or bool(self.acPortStatus) or bool(self.v12PortStatus)):
            return True
        else:
            return False
            
    def setDirectConnectMode(self):
        commandString = f"curl --location --request POST \"http://{self.urlDomain}/join-direct\" --data \"\""
        try:
            subprocess.run([commandString],shell=True)
            self.logAndPrintMsg("Ran setDirectConnectMode() command")
        except subprocess.CalledProcessError as err:
            self.logAndPrintMsg("Exception while running setDirectConnectMode() command")
            sys.exit(1)
            
    def createSOCDequeWithLength(self, x):
        self.socDequeContainer = collections.deque(maxlen=x)
    
    def updateSOCDequeWithValue(self, x):
        self.socDequeContainer.append(x)

    def runningMeanOfSOCDequeContainer(self):
        total = sum([float(elem) for elem in self.socDequeContainer])
        return float("{0:.2f}".format(total/SOC_DEQUE_CONTAINER_LEN))

    def updateStatus(self):
        success = False
        getStateString = 'http://%s/state' % self.urlDomain
        firstUpdate = False
        
        if self.socDequeContainer is None:
            self.createSOCDequeWithLength(SOC_DEQUE_CONTAINER_LEN)
            firstUpdate = True
            
        try:
            response = requests.get(getStateString)
            response.raise_for_status()
            yetiStateJSON = response.json()
        
            print("Updating Yeti status...")
            for key, value in yetiStateJSON.items():
                setattr(self,key,value)
            success = True

            # on first update, preload deque with current socPercent to avoid
            # discontinuity at start of soc graph
            if (firstUpdate):
                for x in range(SOC_DEQUE_CONTAINER_LEN):
                    self.socDequeContainer.append(self.socPercent)
                firstUpdate = False
            else:
                self.updateSOCDequeWithValue(self.socPercent)
 
            self.averageSOCPercent = self.runningMeanOfSOCDequeContainer()            
                
        except requests.exceptions.HTTPError as err:
            self.logAndPrintMsg("**** HTTPError on request to Yeti.")
            self.logAndPrintMsg(err)
        except requests.exceptions.Timeout as err:
            # Maybe set up for a retry, or continue in a retry loop
            self.logAndPrintMsg("**** Request to Yeti timed out.")
            self.logAndPrintMsg(err)
        except requests.exceptions.TooManyRedirects as err:
            self.logAndPrintMsg("**** URL error in connecting to Yeti.")
            self.logAndPrintMsg(err)
        except requests.exceptions.RequestException as err:
            self.logAndPrintMsg("**** Error connecting to Yeti.")
            self.logAndPrintMsg(err)
        
        return success
        
    def setBacklight(self, state):
        if (state == "ON"):
            onOff = 1
        else:
            onOff = 0
            
        commandString = f"curl --location --request POST \"http://{self.urlDomain}/state\" \
         --header \"Content-Type: application/json\" --data \"{{backlight: {onOff}}}\""

        try:
            subprocess.run([commandString],shell=True)
            self.logAndPrintMsg("Ran setBacklight() command")
            self.updateStatus()
        except subprocess.CalledProcessError as err:
            self.logAndPrintMsg("Exception while running setBacklight() command")
        
    def setPort(self, port, state):
        # I'd prefer to run command using a python request, but not sure how to handle the 
        # --location arg. Seems to have no effect with a request other than hang the call 
        #         header = {'Content-Type':'application/json'}
        #         payload = {'acPortStatus': 1, 'v12PortStatus': 0, 'usbPortStatus': 0}
        #         r = requests.post('http://172.16.42.20/state', headers = header, data = payload)
        #         print(r.status_code)
        
        if (port == YetiPort.USB):
            portString = "usbPortStatus"
        elif (port == YetiPort.V12):
            portString = "v12PortStatus"
        elif (port == YetiPort.AC):
            portString = "acPortStatus"
            
        if (state == "ON"):
            onOff = 1
        else:
            onOff = 0
            
        commandString = f"curl --location --request POST \"http://{self.urlDomain}/state\" \
         --header \"Content-Type: application/json\" --data \"{{\"{portString}\": {onOff}}}\""

        try:
            subprocess.run([commandString],shell=True)
            self.logAndPrintMsg("Ran setPort() command")
            self.updateStatus()
        except subprocess.CalledProcessError as err:
            self.logAndPrintMsg("Exception while running setPort() command")

    def currentState(self):
        return f"Yeti: Current SOC: {str(self.socPercent)}, Running Ave SOC: {str(self.averageSOCPercent)}, Watts in:{self.wattsIn}, Watts out:{self.wattsOut}"

    def to_json(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)
        
class FreezerRelay:
    def __init__(self, controlPin):
        # with active_high = True, the on() method sets the pin to HIGH
        self._freezerRelay = OutputDevice(controlPin, active_high = True, initial_value=False)
        
    def turnOnWallCurrent(self):
        self._freezerRelay.on()
        
    def turnOffWallCurrent(self):
        self._freezerRelay.off()
        
    def wallCurrentIsOn(self):
        if (bool(self._freezerRelay.value)):
            return True
        else:
            return False

    def currentState(self):
        return f"FreezerRelay: Relay is on: {self.wallCurrentIsOn()}"
