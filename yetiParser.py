#!/usr/bin/env python3

# yetiParser.py
import sys
import os
import socket
import threading
import RPi.GPIO as GPIO
import time
import datetime
from datetime import datetime as dt
import logging
from logging import FileHandler
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler 
import json
import requests
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.events import EVENT_JOB_MAX_INSTANCES
import subprocess
from YetiClasses import Yeti1400, YetiPort, FreezerRelay
import email
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

###############################################################################
# Design notes:
#
# We now use a relay to turn on wall current to the freezer when:
# - Yeti's battery charge is too low or too high: We want the batteries to run about 65%-80% full
# - When the manual override switch is in the 'dumpload' position, since no solar power then reaches the Yeti
# - There is a provision for a 'use dumpload' switch, currently commented out. See DUMP_LOAD_SWITCH

# ToDo
# - consider providing an 'outputChannel' method to report current channel
# - reinstitute physical dumpload--perhaps to a battery, pump, light, etc. to use up
#   solar energy when battery is beyond BATTERY_CHARGE_CEILING
###############################################################################

# Declare global variables and constants
scheduler = None
myYeti = None
freezerACRelay = None
debugLogger = None
loadOnStartTime = dt.today() # giving it a value, in case Yeti starts in load On state

## Logging
headerString ='DateAndTime,PercentCharged,WattsIn,WattsOut'
current_sys_stat_log_file = 'currentSystemStatus.log'  
debugLogFile = 'yetiParserDebug.log'
basedir = os.path.dirname(os.path.realpath(__file__))
logdir = os.path.join(basedir, 'yetiSystemLogs')
current_sys_stat_path = logdir + "/" + current_sys_stat_log_file

# cookie file to tell solarStatusImageGen.py to create a 'yesterdayStatus' image
cookieFilePath = os.path.join(basedir, 'createYesterdaySummaryImage')

# error recovery
emailQuiescentTimer = None
EMAIL_QUIESCENT_PERIOD_ACTIVE = False # used to notify about WiFi problems
EMAIL_QUIESCENT_PERIOD_PERIOD = 300  # seconds
portResetQuiescenceTimer = None
PORT_RESET_QUIESCENT_PERIOD_ACTIVE = False
PORT_RESET_QUIESCENT_PERIOD = 1200  # seconds
wifiRestartAttemptNum = 0
wifiRestartWindowBegin = None
WIFI_RESTART_WINDOW_DURATION = 12  # minutes. After 10mins, the Yeti stops communicating
YET_IS_OFFLINE = False

# If we turn on the 12v out to the freezer just as we pass the battery charge floor, 
# the battery level descends below the floor again--back and forth until the sun is strong enough.
# By adding a delay, we give the solar panels about 20 minutes or so to do there work before 
# checking for charge level again.
freezerPowerChangeQuiescentTimer = None
FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE = False
# Matt!: Shouldn't I have an OUTPUT_CHANNEL_QUIESCENT_PERIOD separate from PORT_RESET_QUIESCENT_PERIOD?
# right now, I'm using PORT_RESET_QUIESCENT_PERIOD for both operations

YETI_IS_SUPPLYING_FREEZER = False

# various
BATTERY_CHARGE_CEILING = 106  # when the charge is between these numbers, the Yeti...
BATTERY_CHARGE_FLOOR = 55     # runs the freezer and AC is turned off (except, see dumpload switch)
USE_DUMP_LOAD = True          # divert power to dumpload once Yeti is topped off
URL_DOMAIN = "10.0.1.60"
BACKGROUND_JOB_INTERVAL = 90 #seconds
MAIN_LOOP_SLEEP_INTERVAL = 30 #seconds
MANUAL_OVERRIDE_SETTING = "Auto" # states are "Auto", "Dumpload", "Yeti" to direct output


# GPIO pin numbers 
FREEZER_AC_RELAY = 21
DUMP_LOAD_RELAY = 26
YETI_LOAD_RELAY = 19
# DUMP_LOAD_SWITCH = 22 # selects whether dumpload output is used MATT!! not currently connected
MANUAL_OVERRIDE_DUMPLOAD_SWITCH = 9 # One side of three-way switch, off implies Auto
MANUAL_OVERRIDE_YETI_SWITCH = 10    # Other side of three-way switch, off implies Auto

######## -------------- Utilities -------------------------
def timeString():
    now = datetime.datetime.now()
    return(now.strftime("%A, %d. %B %Y %I:%M:%S %p"))

def makeDirIfNeeded(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
def CtoF(c):
    return 9.0/5.0 * float(c) + 32
    
# Ts to stdout and debug log file
def logAndPrintMsg(msg):
    debugLogger.info(msg)
    print(msg)

def emailYetiWifiSituation(): 
    timestamp = dt.today().strftime("%Y/%m/%d %H:%M:%S.%f")
    subjectLine = "Yeti wifi problem"
    messageBody = f"{timestamp}: Yeti is not reachable by wifi. Switching freezer to AC power."
    throttledEmailMessage(subjectLine, messageBody)

# If all fails, turn on AC, send email, stop trying to contact Yeti.
def yetiOfflineHandler():
    global YET_IS_OFFLINE
    
    YET_IS_OFFLINE = True
    logAndPrintMsg(f"        - switching freezer to AC and emailing")
    turnOnFreezerACRelay()
    emailYetiWifiSituation()

def _restartWifiNetwork(): # this function does the work. Called by restartWifiNetwork():
    commandStringDown = f"sudo ifconfig wlan0 down"
    commandStringUp = f"sudo ifconfig wlan0 up"
    
    logAndPrintMsg(f"!!! Lost WiFi network!")

    try:
        subprocess.run([commandStringDown],shell=True)
        debugLogger.info("Ran sudo ifconfig wlan0 down")
    except subprocess.CalledProcessError as err:
        debugLogger.info("Exception while running 'sudo ifconfig wlan0 down' command")
        
    time.sleep(10) 

    try:
        subprocess.run([commandStringUp],shell=True)
        debugLogger.info("Ran sudo ifconfig wlan0 up")
    except subprocess.CalledProcessError as err:
        debugLogger.info("Exception while running 'sudo ifconfig wlan0 up' command")

def restartWifiNetwork():  # this evaluates whether it is worth trying to restart wifi
    global wifiRestartAttemptNum, wifiRestartWindowBegin
    
    currentTime = datetime.datetime.now()
    
    # first, check whether we can reach a well-known site
    if (canReachInternet):
        logAndPrintMsg("***** restartWifiNetwork(): rPi is connected to internet")
        # MATT: more work: if rPi is connected, then this is a Yeti problem
        #       and we don't need to fiddle the rPi's settings.
    else:
        logAndPrintMsg("***** restartWifiNetwork(): rPi is NOT connected to internet")
    
    if (wifiRestartAttemptNum == 0): # first attempt
        wifiRestartAttemptNum = 1
        wifiRestartWindowBegin = datetime.datetime.now()
        logAndPrintMsg(f"first attempt to restart. Time: {wifiRestartWindowBegin}")
        _restartWifiNetwork()
    else:
        timeDifference  = (currentTime - wifiRestartWindowBegin)
        elapsedMins = (timeDifference.total_seconds() / 60)
        logAndPrintMsg(f"Wifi reset window opened {elapsedMins} minutes ago.")
        
        if (elapsedMins > WIFI_RESTART_WINDOW_DURATION):
            logAndPrintMsg(f"    Unsuccessful in reaching Yeti by wifi")
            logAndPrintMsg(f"        - wifi restart attempts: {wifiRestartAttemptNum}")
            yetiOfflineHandler()
        else:
            _restartWifiNetwork()
            wifiRestartAttemptNum = wifiRestartAttemptNum + 1

def canReachInternet():
    try:
        # connect to the host -- tells us if the host is actually reachable
        socket.create_connection(("www.google.com", 80))
        return True
    except OSError:
        pass
    return False

######## -------------Program Setup-----------
def setupGPIO():
    # set-up input pin and interrupt
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(DUMP_LOAD_RELAY, GPIO.OUT)
    GPIO.setup(YETI_LOAD_RELAY, GPIO.OUT)
    
    GPIO.setup(MANUAL_OVERRIDE_DUMPLOAD_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(MANUAL_OVERRIDE_YETI_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(MANUAL_OVERRIDE_DUMPLOAD_SWITCH, GPIO.BOTH, callback=manualOverrideSwitchAction, bouncetime=100)
    GPIO.add_event_detect(MANUAL_OVERRIDE_YETI_SWITCH, GPIO.BOTH, callback=manualOverrideSwitchAction, bouncetime=100)

def schedulerEventListener(event):
    # MATT: Need to handle exception--restart network, email, etc. For now, just report
    logAndPrintMsg('    *********** BackgroundScheduler: MAX job instances reached!!! *******************')
    logAndPrintMsg(f'    ***********  event code:  {event.code} *******************')
    logAndPrintMsg(f'    ***********  job id: {event.job_id} *******************')
    logAndPrintMsg(f'    ***********  sched. runs: {event.scheduled_run_times} *******************')

def setupScheduler():
    global scheduler
    
    # Attempting to keep Yeti from ending direct-connect mode
    directConnectUpdateInterval = (BACKGROUND_JOB_INTERVAL * 2) + 30 # the offset (30) keeps the two http requests separate
    
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=updateYetiStatus, trigger="interval", seconds=BACKGROUND_JOB_INTERVAL, max_instances=4, id="Yeti_State")
    scheduler.add_job(func=updateYetiDirectConnect, trigger="interval", seconds=directConnectUpdateInterval, max_instances=4, id="Yeti_DirectConnect_Update")

    # see https://apscheduler.readthedocs.io/en/latest/modules/events.html#module-apscheduler.events
    # for types of events to OR together, e.g. EVENT_JOB_EXECUTED | EVENT_JOB_ERROR
    scheduler.add_listener(schedulerEventListener, EVENT_JOB_MAX_INSTANCES)
    scheduler.start()
    
def createFreezerACRelayObject(relayPin):
    global freezerACRelay
    
    freezerACRelay = FreezerRelay(relayPin)

def createYetiObjectWithURLDomain(domain):
    global myYeti

    try:
        getStateString = 'http://%s/state' % domain
        response = requests.get(getStateString)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        debugLogger.info(err)
        sys.exit(1)
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        logAndPrintMsg("Request to Yeti timed out.")
        sys.exit(1)
    except requests.exceptions.TooManyRedirects:
        logAndPrintMsg("URL error in connecting to Yeti.")
        sys.exit(1)
    except requests.exceptions.RequestException as e:
        logAndPrintMsg("Error connecting to Yeti.")
        sys.exit(1)
        
    yetiState = json.loads(json.dumps(response.text))
    myYeti = Yeti1400.from_json(yetiState)
    myYeti.setURLDomain(URL_DOMAIN)
    myYeti.setDebugLogger(debugLogger)
    myYeti.setDirectConnectMode()
            
    if (myYeti is  None):
        logAndPrintMsg("Cannot create Yeti object...")
        raise
    else:
        logAndPrintMsg ("Contacted Yeti")

######## -------------Logging-----------------
class MyTimedRotatingFileHandler(TimedRotatingFileHandler):
    def __init__(self, logfile, when, interval, backupCount):
        super(MyTimedRotatingFileHandler, self).__init__(logfile, when, interval, backupCount)
        self._header = ""
        self._cookieFile = ""
        self._log = None

    def doRollover(self):
        open(self._cookieFile, 'w').close()
        super(MyTimedRotatingFileHandler, self).doRollover()
        if self._log is not None and self._header != "":
            self._log.info(self._header)

    def setCookieFile(self, cookieFile):
        self._cookieFile = cookieFile

    def setHeader(self, header):
        self._header = header

    def configureHeaderWriter(self, header, log):
        self._header = header
        self._log = log
        
def getCurrentSysStatLogger(name, log_path, level=logging.INFO):
    global headerString, cookieFilePath
    
    # only write header if we're starting a new file
    writeHeader = False
    if not os.path.exists(log_path):
        writeHeader = True
        debugLogger.info("creating new current usage log at " + log_path)
    

    handler = MyTimedRotatingFileHandler(log_path,
                    when="midnight", # type of interval, S, M, H, D, 'midnight' 
                    interval=1, # product of 'when' and 'interval' determines frequency
                    backupCount=2) # how many backups to save
                    
    rotLogger = logging.getLogger(name)
    rotLogger.setLevel(level)
    handler.configureHeaderWriter(headerString, rotLogger)
    handler.setCookieFile(cookieFilePath)
    rotLogger.addHandler(handler)
    
    if writeHeader:
        rotLogger.info(headerString) # write initial header
        
    return rotLogger

def getDebugLogger():
    global debugLogFile
    global debugLogger
    
    logLevel = logging.INFO
    logFormat = '%(asctime)s %(levelname)s %(message)s'

    debugLogger = logging.getLogger("Debug Log")
    debugLogger.setLevel(logLevel)
    debugLoggerFilehandler = FileHandler(debugLogFile)
    debugLoggerFilehandler.setLevel(logLevel)
    debugLoggerFilehandler.setFormatter(Formatter(logFormat))
    debugLogger.addHandler(debugLoggerFilehandler)    
    return debugLogger
    
def endQuiescentTimerForEmail():
    global emailQuiescentTimer, EMAIL_QUIESCENT_PERIOD_ACTIVE
    EMAIL_QUIESCENT_PERIOD_ACTIVE = False
    emailQuiescentTimer.cancel()

# throttles messaging to one email per x seconds
def throttledEmailMessage(subjectLine, messageBody):
    global emailQuiescentTimer, EMAIL_QUIESCENT_PERIOD_ACTIVE
    
    if (not EMAIL_QUIESCENT_PERIOD_ACTIVE):
        EMAIL_QUIESCENT_PERIOD_ACTIVE = True
        emailQuiescentTimer = threading.Timer(EMAIL_QUIESCENT_PERIOD_PERIOD, endQuiescentTimerForEmail)
        emailQuiescentTimer.start()
        emailMessage(subjectLine, messageBody)

def emailMessage(subjectLine, messageBody):    
    fromAddress = "mattronome1@gmail.com"
    # recipients = ['matt@mac.com', 'smattmann@mac.com']
    recipients = ['matt@mac.com']

    msg = MIMEMultipart()
    msg['From'] = fromAddress
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = subjectLine

    body = messageBody
    msg.attach(MIMEText(body, 'plain'))
    
    try:
        server = None
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login("mattronome1@gmail.com", "peblpxstyceqtmpv")
        text = msg.as_string()
        server.sendmail(fromAddress, recipients, text)
    except smtplib.SMTPException as e:
        print('Error: Unable to send email: %s\n' % e)
    except socket.gaierror as e:
        print('Error: Unable to send email. Socket error: %s\n' % e)
    finally: # this executes as the last statement in the try clause, with/out exceptions
        if (server is not None):
            server.quit()

def endQuiescentTimerForPortReset():
    global portResetQuiescenceTimer, PORT_RESET_QUIESCENT_PERIOD_ACTIVE
    
    PORT_RESET_QUIESCENT_PERIOD_ACTIVE = False
    portResetQuiescenceTimer.cancel()
    logAndPrintMsg('***************** ending quiescent period for port reset')

def toggle12vPortOn():
    myYeti.setPort(YetiPort.V12, "OFF")
    time.sleep(20) # attempting to turn it ON directly resulted in it turning off, so a pause
    myYeti.setPort(YetiPort.V12, "ON")

def handleOutputPortProblem():
    global portResetQuiescenceTimer, PORT_RESET_QUIESCENT_PERIOD_ACTIVE

    timestamp = dt.today().strftime("%Y/%m/%d %H:%M:%S.%f")

    if (myYeti.v12PortStatus == 0) and (YETI_IS_SUPPLYING_FREEZER == True):
        if (not PORT_RESET_QUIESCENT_PERIOD_ACTIVE):
            PORT_RESET_QUIESCENT_PERIOD_ACTIVE = True
            portResetQuiescenceTimer = threading.Timer(60.0, endQuiescentTimerForPortReset) # corrective action once per minute
            portResetQuiescenceTimer.start()
            toggle12vPortOn()
            subjectLine = "Yeti port problem"
            messageBody = f"{timestamp}: Yeti should be supplying freezer but 12v port status: {myYeti.v12PortStatus}. Attempting to reset."
            emailMessage(subjectLine, messageBody)

    if (myYeti.v12PortStatus == 2):  # need to throttle the messages: one a minute, for example.
        if (not PORT_RESET_QUIESCENT_PERIOD_ACTIVE):
            PORT_RESET_QUIESCENT_PERIOD_ACTIVE = True
            portResetQuiescenceTimer = threading.Timer(60.0, endQuiescentTimerForPortReset)
            portResetQuiescenceTimer.start()
            toggle12vPortOn()
            subjectLine = "Yeti port problem"
            messageBody = f"{timestamp}: Yeti's 12v port status: {myYeti.v12PortStatus}. Attempting to reset."
            emailMessage(subjectLine, messageBody)

def logCurrentState(sysStatLogger):
    timestamp = dt.today().strftime("%Y/%m/%d %H:%M:%S.%f")

    logAndPrintMsg(f"----- {timeString()} ----")
    # headerString ='DateAndTime,PercentCharged,WattsIn,WattsOut'
    logMsg = f"{timestamp},{str(myYeti.averageSOCPercent)},{myYeti.wattsIn},{myYeti.wattsOut}"
    sysStatLogger.info(logMsg)

def updateYetiDirectConnect():
    global myYeti
    
    if not YET_IS_OFFLINE:
        logAndPrintMsg(f"----- updateYetiDirectConnect() ----")
        myYeti.setDirectConnectMode()

######## -------------Yeti Status-----------------
# This function is invoked by background scheduler every BACKGROUND_JOB_INTERVAL seconds
def updateYetiStatus():
    global myYeti, wifiRestartAttemptNum, wifiRestartWindowBegin, YET_IS_OFFLINE
    
    if not YET_IS_OFFLINE:
        success = myYeti.updateStatus()
        
        if (not success):
            restartWifiNetwork() # this will be attempted several times, and lacking success, YET_IS_OFFLINE is set to True
        else: # wifi is on-line (or back on-line), so reset restart state
            wifiRestartWindowBegin = None
            wifiRestartAttemptNum = 0
            # check for port problems
            if ((myYeti.v12PortStatus == 2) or (myYeti.v12PortStatus == 0)): 
                handleOutputPortProblem()
    else: # we've attempted but failed to get yeti online, email has been sent in restartWifiNetwork()
        pass
    
def yetiChargeStateString():
    global myYeti
    
    #outFlow = myYeti.ampsOut - myYeti.ampsIn
    outFlow = myYeti.wattsOut - myYeti.wattsIn
    netFlowString = "steady state"
    if (outFlow > 0):
        netFlowString = "discharging"
    elif (outFlow < 0):
        netFlowString = "charging"
    return netFlowString

######## -------------Managing Yeti and Dumpload-----------------
def initializeOutputChannel(): # when program first starts
    global myYeti, MANUAL_OVERRIDE_SETTING
    manualOverrideSwitchAction(None) # gets switch setting
    updateOutputChannel()

def endQuiescentTimerForFreezerSupplyChange():
    global freezerPowerChangeQuiescentTimer, FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE
    
    FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE = False
    freezerPowerChangeQuiescentTimer.cancel()
    logAndPrintMsg('                          *** Ending quiescent period for output channel change')

def startQuiescentTimerForFreezerSupplyChange():
    global freezerPowerChangeQuiescentTimer, FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE, PORT_RESET_QUIESCENT_PERIOD

    FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE = True
    freezerPowerChangeQuiescentTimer = threading.Timer(PORT_RESET_QUIESCENT_PERIOD, endQuiescentTimerForFreezerSupplyChange) # should have separate period value
    freezerPowerChangeQuiescentTimer.start()
    logAndPrintMsg('                          *** Starting quiescent period for output channel change')


# Solar power can go to either the Yeti batteries or to some dumpload.
def updateOutputChannel():
    global myYeti, MANUAL_OVERRIDE_SETTING
    
    if (MANUAL_OVERRIDE_SETTING == "Auto"):
        turnOffDumpLoad() # for now, just default to sending power to the Yeti
    elif (MANUAL_OVERRIDE_SETTING == "Yeti"):
        logAndPrintMsg("     ----> updateOutputChannel(): MANUAL_OVERRIDE_SETTING is Yeti.")
        turnOffDumpLoad()
    elif (MANUAL_OVERRIDE_SETTING == "Dumpload"):
        logAndPrintMsg("     ----> updateOutputChannel(): MANUAL_OVERRIDE_SETTING is Dumpload.")
        turnOnDumpLoad()

def updateFreezerPowerSource():
    global myYeti, YETI_IS_SUPPLYING_FREEZER, FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE
    
    # logAndPrintMsg(f"        socPercent: {myYeti.socPercent}, ave.soc:{str(myYeti.averageSOCPercent)},  bat. ceiling: {BATTERY_CHARGE_CEILING}, floor: {BATTERY_CHARGE_FLOOR}")
    if FREEZER_SUPPLY_CHANGE_QUIESCENT_PERIOD_ACTIVE:
        logAndPrintMsg("     ----> Quiescent period for change to freezer supplier. No change for now.")
    else:
        
        if (myYeti.averageSOCPercent > BATTERY_CHARGE_FLOOR) and (myYeti.averageSOCPercent < BATTERY_CHARGE_CEILING):
            turnOffFreezerACRelay() # run freezer from batteries and solar
            YETI_IS_SUPPLYING_FREEZER = True
        else:
            turnOnFreezerACRelay()
            YETI_IS_SUPPLYING_FREEZER = False


# allow for manual override of output direction
def manualOverrideSwitchAction(channel):
    global MANUAL_OVERRIDE_SETTING
    
    if GPIO.input(MANUAL_OVERRIDE_DUMPLOAD_SWITCH):
        MANUAL_OVERRIDE_SETTING = "Dumpload"
        logAndPrintMsg("## manualOverrideSwitchAction: MANUAL_OVERRIDE_SETTING is Dumpload ")
    elif GPIO.input(MANUAL_OVERRIDE_YETI_SWITCH):
        MANUAL_OVERRIDE_SETTING = "Yeti"
        logAndPrintMsg("## manualOverrideSwitchAction: MANUAL_OVERRIDE_SETTING is Yeti")
    else:
        MANUAL_OVERRIDE_SETTING = "Auto"
        logAndPrintMsg("## manualOverrideSwitchAction: MANUAL_OVERRIDE_SETTING is Auto")
        
    updateOutputChannel()

def dumploadIsOn():
    if (GPIO.input(DUMP_LOAD_RELAY)):
        return True
    else:
        return False
        
def yetiloadIsOn():
    if (GPIO.input(YETI_LOAD_RELAY)):
        return True
    else:
        return False

def wallCurrentIsOn():
    global freezerACRelay
    return  freezerACRelay.wallcurrentIsOn()

def printLoadOnDuration():
    global loadOnStartTime
    now = dt.today()
    timeInterval = now - loadOnStartTime
    logAndPrintMsg(f"     ====> Load On duration: {timeInterval.seconds} seconds")

# Turning on the dumpload turns off power to the Yeti
def turnOnDumpLoad():
    global myYeti, loadOnStartTime

    if (not dumploadIsOn()):
        logAndPrintMsg(f"    Turning load on. Running ave charge level is {str(myYeti.averageSOCPercent)}")
        loadOnStartTime = dt.today()
        GPIO.output(DUMP_LOAD_RELAY, GPIO.HIGH)
        GPIO.output(YETI_LOAD_RELAY, GPIO.LOW)
    
def turnOffDumpLoad():
    global myYeti
    
    if (dumploadIsOn()):
        logAndPrintMsg(f"    Turning load on. Running ave charge level is {str(myYeti.averageSOCPercent)}")
        printLoadOnDuration()
        GPIO.output(DUMP_LOAD_RELAY, GPIO.LOW)
        GPIO.output(YETI_LOAD_RELAY, GPIO.HIGH)
        myYeti.setBacklight("OFF") # Apparently, turning on charging sets the backlight on. Turning back off to save power.

def turnOnFreezerACRelay():
    global freezerACRelay
    
    if (not freezerACRelay.wallCurrentIsOn()):
        startQuiescentTimerForFreezerSupplyChange()
        freezerACRelay.turnOnWallCurrent() # freezer automatically switches to AC, if available
        logAndPrintMsg(f"                          --> Turning on AC current")
    
def turnOffFreezerACRelay():
    global freezerACRelay
    
    if (freezerACRelay.wallCurrentIsOn()):
        startQuiescentTimerForFreezerSupplyChange()
        freezerACRelay.turnOffWallCurrent() # freezer automatically switches to Yeti when AC isn't available
        logAndPrintMsg(f"                          --> Turning off AC current")
    
######## -------------Main-----------------
def main():
    global scheduler, myYeti, freezerACRelay, loadOnStartTime, logdir, debugLogger
    global USE_DUMP_LOAD, MANUAL_OVERRIDE_SETTING, YET_IS_OFFLINE
    
    makeDirIfNeeded(logdir)
    debugLogger = getDebugLogger()
    currentSysStatLogger = getCurrentSysStatLogger('rotating_current_sys_stat_logger', current_sys_stat_path)
    
    try:
        setupGPIO()

        if not canReachInternet():
            logAndPrintMsg("***** rPi is NOT connected to internet *****")
            logAndPrintMsg("                 exiting     ")
            quit()

        createFreezerACRelayObject(FREEZER_AC_RELAY)
        createYetiObjectWithURLDomain(URL_DOMAIN)
        
        # initially, direct solar power to Yeti. Override this only if physical switch
        # is set to "Dumpload", See updateOutputChannel()
        GPIO.output(YETI_LOAD_RELAY, GPIO.HIGH)
        
        # get status, and start scheduler to report status
        updateYetiStatus() # loads initial values, such as state of charge
        setupScheduler()
        
        # find out status of switches
        initializeOutputChannel()
#         USE_DUMP_LOAD = GPIO.input(DUMP_LOAD_SWITCH)
#         logAndPrintMsg(f"Dumpload switch state is: {USE_DUMP_LOAD}")
        logAndPrintMsg(f"Initial setting for MANUAL_OVERRIDE_SETTING is: {MANUAL_OVERRIDE_SETTING}")


        while True:
            if YET_IS_OFFLINE:
                logAndPrintMsg(f"***** Yeti is offline. {freezerACRelay.currentState()}")
            else:
                logCurrentState(currentSysStatLogger)

                if (myYeti is not None):
                    # logAndPrintMsg(f"   Yeti batteries are {yetiChargeStateString()}.")
                    updateOutputChannel() # whether solar power goes to Yeti or dumpload
                    updateFreezerPowerSource() # whether Yeti or AC will supply freezer
                    
                else:
                    logAndPrintMsg("No Yeti object to inspect...")
                    quit()
                
                logAndPrintMsg(f"   {myYeti.currentState()}")
                logAndPrintMsg(f"   {freezerACRelay.currentState()}")
                
            time.sleep(MAIN_LOOP_SLEEP_INTERVAL) # keeps app from exiting; background scheduler does all the work
            
    except (KeyboardInterrupt, SystemExit):
        print("KeyboardInterrupt, SystemExit error :", sys.exc_info()[0])
        quit()
        
    finally:
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()
        if (scheduler is not None):
            scheduler.shutdown()

# make main() the entry point of this script
if __name__ == '__main__':
    main()


