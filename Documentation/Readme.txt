Readme
Yeti Automation Project
June 3, 2019

Overview
-----------------------
The goal of this project is to create a system that keeps the Yeti1400 charged to a reasonable state, and then--if there's excess input from a solar panel or other source--makes use of that power by turning on one of the Yeti's ports and dumping that power to a load. 

The system in my case (at least initially) consists of a solar panel outside my workshop connected to the Yeti 1400 inside the shop. When there's excess power, the program turns on the AC port and dumps the power to a small electric heater--since the basement shop is always in need of heating.

Comments and Warnings
-----------------------
The code is provided with no guarantees, to use at your own risk. I'm doing this as a hobby and I'm not expert with Python, so there are no doubt bugs and better ways to do things. If you'd like to contribute to improving this project, please do!

There are also many unknowns, such as:

- What are the best values to set for charge and discharge limits? For example, should the app let the Yeti deplete to 80%, 50%, 30%, etc. before turning off the load?

- What effect will this app have on the lifespan of the Yeti 1400?

- The Buck Converter that I purchased works for me, but I make no claims about its appropriateness or safety.


System Hardware
-----------------------
- Yeti 1400
- Solar Panel: Used 310W panel (Vmp:36.38V, Imp:8.52A) 
The Yeti requires a charging voltage of 22V or less, so I added a Buck Converter to step the voltage down.
- Buck Converter: DC-DC Buck Converter, DROK Step Down Voltage Regulator
(https://www.amazon.com/gp/product/B01MSJQAKY/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1). I set the output voltage to 18V.
- 10 AWG Solar panel wire
- Inline Circuit breaker.
- I made up a cable to go from the Buck Converter to the Yeti, adding Powerpole connectors at the Yeti end.
- Raspberry Pi computer with WiFi. (I'm using a Model 3 B+) Of course, you can use any computer to control the Yeti. In fact, I developed the software on a Mac and then moved it onto a rPi for deployment. The rPi has the advantage of being cheap and using very little electricity itself. I can run it from one of the Yeti's USB ports.


Preparing the Raspberry Pi
---------------------------
- My rPi is running Stretch (Version: April 2019, Kernel:4.14)

- The program uses features of Python 3.6 and later (ex. f-strings), so make sure your version is up to date. To install python 3, you might find these instructions useful: https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f

- You may have to install these libraries, if they are not already present:

   - sudo pip3 install requests
   - sudo pip3 install apscheduler
   
- If you want to use the graphing capabilities in the separate script 'yetiStatusImageGen.py' you'll need to install matplotlib and pandas (which can take some time to install)

   - sudo pip3 install matplotlib
   - sudo pip3 install pandas
   
- Finally, if you want to make the graphs accessible via a webserver, install tornado:

    - sudo pip3 install tornado

The versions I'm using are:
    requests: 2.12.4
    apscheduler: 3.6.0
    matplotlib: 3.0.3
    pandas: 0.24.2


Preparing the Yeti 1400
--------------------------
- To start, set the Yeti into Pairing mode. Hold the WiFi button until the WiFi icon on the screen starts to pulse. (You can review the mode indications here: https://s3-us-west-2.amazonaws.com/yetiapp-dev-distribution-content/yeti-wifi-modes-guide.html ).

- Use the Goal Zero app to put the Yeti on your wifi network.

- Find the IP address of the Yeti. I use the "Who's on My WiFi" application to scan for the new client and get its IP. For example, 10.0.1.60

- Check that you can contact the Yeti directly. Open a browser window on your computer and enter the address of the Yeti's webserver. 

   ex. http://10.0.1.60

If you connect, the browser window slowly loads the words "Power. Anything. Anywhere."

- Next, in a Terminal window use the POST Join-Direct endpoint to set the Yeti into Direct-Connect mode. If you don't do this, the Access Point will timeout in about 10 minutes. 

     curl --location --request POST "http://10.0.1.60/join-direct" --data ""

> curl --location --request POST "http://172.16.42.20/join-direct" --data ""
{ "name": "yeti30aea43b817c", "model": "Yeti 1400", "firmwareVersion": "0.7.2", "macAddress": "30aea43b817c", "platform": "esp32", "state": {
  "thingName": "yeti30aea43b817c",
  "v12PortStatus": 0,
  "usbPortStatus": 0,
  "acPortStatus": 0,
  "backlight": 1,
  "app_online": 1,
  "wattsIn": 0.0,
  "ampsIn": 0.0,
  "wattsOut": 0.0,
  "ampsOut": 0.0,
  "whOut": 0,
  "whStored": 1400,
  "volts": 12.3,
  "socPercent": 104,
  "isCharging": 0,
  "timeToEmptyFull": -1,
  "temperature": 17,
  "wifiStrength": -41,
  "timestamp": 188789,
  "firmwareVersion": "0.7.2",
  "version": 2
} }

Note that while the Yeti is in the Direct-Connect state, it cannot perform Over The Air firmware updates and you will also not be able to view/control your Yeti with our app over the cloud (i.e. off the local network). But you could be in direct-connect with more than one client device. While in Direct-Connect, if the Yeti does not receive any HTTP requests for more than 30 hours, it will timeout and the Wifi will be disabled. The yetiParser.py script takes care of this by periodically sending HTTP requests to measure the Yeti's state.

5. You are now ready to launch the yetiParser.py script.

If your rPi has a keyboard and screen, open a Terminal window, cd to the directory with the two scripts (yetiParser.py and yetiClasses.py), and issue the command (make sure you're using python 3.6 or later):

    > python3 yetiParser.py

You can also run the rPi headlessly (as I do) and use VNC to display its virtual screen on your desktop computer's screen. The yetiParser.py script queries the Yeti every minute or so for its state of charge, input and output watts, etc. and logs the data to a file (yetiSystemLogs/currentSystemStatus.log)

To generate graphs of power input/output and battery level, open a Terminal window and run:

    > python3 yetiStatusImageGen.py
    
This script periodically reads the currentSystemStatus.log file, and generates a graph from the data, writing the graph as images/currentYetiSystemStatus.png

Finally, to vend those graphs by webserver, in another Terminal, run:

    > python3 yetiImageServer.py

This script listens for HTTP requests on a port (ex. 172.16.42.20:8888) and vends the images requested.

Miscellaneous Information
---------------------------------
Turn on/off backlight on LED:

curl --location --request POST "http://10.0.1.60/state" --header "Content-Type: application/json" --data "{backlight:0}"

Energy usage with Raspberry Pi 3 B+

  "backlight": 0,
  "app_online": 1,
  "wattsIn": 0.0,
  "ampsIn": 0.0,
  "wattsOut": 3.4,
  "ampsOut": 0.3,
 
