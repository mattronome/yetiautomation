# utility to turn on solar relay
# written when Yeti would not communicate with yetiParser.py

import argparse
import RPi.GPIO as GPIO

YETI_LOAD_RELAY = 19

def setupGPIO():
    # set-up input pin and interrupt
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(YETI_LOAD_RELAY, GPIO.OUT)

def yetiloadIsOn():
    if (GPIO.input(YETI_LOAD_RELAY)):
        return "On"
    else:
        return "Off"


def main():
    setupGPIO()
    
    parser = argparse.ArgumentParser(description='Turn Yeti load relay on/off')
    parser.add_argument('--onOff', help='supply On or Off as arg value')
    
    args = parser.parse_args()
    
    if (args.onOff == 'On'):
        print ("Turning Yeti relay On")
        GPIO.output(YETI_LOAD_RELAY, GPIO.HIGH)

    else:
        print ("Turning Yeti relay Off")
        GPIO.output(YETI_LOAD_RELAY, GPIO.LOW)

    print(f"Yeti load relay is: {yetiloadIsOn()}")

# make main() the entry point of this script
if __name__ == '__main__':
    main()



